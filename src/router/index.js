import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/about',
    name: 'about',
   
    component: () => import('../views/AboutView.vue')
  },
  {
    path: '/taze',
    name: 'taze',
   
    component: () => import('../views/AdminUsers.vue')
  },
  {
    path: '/teacher',
    name: 'mugallym',
    component: () => import( '../views/TeacherView.vue')
  },
  {
    path: '/respons',
    name: 'respons',
    component: () => import( '../views/ResponsView.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import( '../views/LoginView.vue')
  },
  {
    path: '/sapak',
    name: 'sapak',
    component: () => import( '../views/SapaklarView.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
